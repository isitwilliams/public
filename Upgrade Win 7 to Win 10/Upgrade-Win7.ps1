# LabTech Specific Stuff. Do not touch. Delete if not using LabTech to deploy.

$LabTechLocationDrive = "%LocationDrive%"
#--------------------------------------------
$Win10Dir = $LabTechLocationDrive + "\Windows1903\"
#--------------------------------------------
#Do not modify code below the line


$MSE = Test-Path "C:\Program Files\Microsoft Security Client\Setup.exe"
if ($MSE -eq $true) {
    & "C:\Program Files\Microsoft Security Client\Setup.exe" /x /s
}

New-PSDrive -Name "P" -PSProvider "FileSystem" -Root $Win10Dir

$setuplocation = "P:\setup.exe /quiet /auto Upgrade /DynamicUpdate enable"

Invoke-Expression $setuplocation