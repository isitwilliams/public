# In Place Upgrade of Windows 7 to Windows 10

## What it Does
This script will mount a directory containing Windows 10 Upgrade files from a network drive onto the Windows 7 machine and then silently upgrade in the background.

Before initiating the upgrade, the script will check to see if Microsoft Security Essentials is installed. It will remove it silently if found.

This will reboot the machine without warning. 

Please review the [cli options](https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/windows-setup-command-line-options) for the upgrade executable and adjust your version to your needs.

## Prerequisites

1. Configure Location Drive for the client site in Automate. This is done inside the Location Page for the client (NOT the Client page).

2. Download the latest Windows 10 (Currently Version 1903) from [here](https://www.microsoft.com/en-us/software-download/windows10) and save it to the location drive.

3. You must extract the ISO's contents to a directory named ```Windows1903```. This is key because Windows 7 cannot natively mount an ISO file.

## Create LabTech Script

The LabTech script should have the following structure and procedures:

IF ```File Check```
* File Path %LocationDrive%\Windows1903\setup.exe

THEN
* Note: File Exists. Upgrading to Windows 10
* Execute Powershell Bypass as local and store result in: @PSOUTPUT@
* LOG: @PSOUTPUT@

ELSE
* Notes: File Does Not Exist. Exiting.
* Script Exit with Error.


In Step two of the ```THEN``` section, paste the code from ```Upgrade-Win7.ps1``` into the "Script to Execute" box.

