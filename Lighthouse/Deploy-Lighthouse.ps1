Import-Module az

$user = ''
$password = ConvertTo-SecureString '' -AsPlainText -Force
if ($user.Length -gt 10){
    $creds = New-Object System.Management.Automation.PSCredential($user,$password)
} else {
    Write-Host "Enter Azure Tenant Admin Credentials (Must have owner role for the tenant's subscription)"
    $creds = Get-Credential
}

Connect-AzAccount -Credential $creds

Function ChooseSubscription {
    $subs = Get-AzSubscription
    Write-Host 'Choose a Subscription'
    For ($i=0; $i -lt $subs.Count; $i++) {
        Write-Host "$($i+1): $($subs[$i].Name)              :              ID: $($subs[$i].Id)"
    }
    [int]$selection = Read-Host "Select the number for the subscription you want. (Microsoft Azure)"
    Write-Host "You selected $($subs[$selection-1])."
    $id = $($subs[$selection-1])
    return $id
}

$id = ChooseSubscription

$sub = Get-AzSubscription -SubscriptionId $id
Set-AzContext $sub

$Locale = Read-Host "What Azure Location is your client in? (Example: CentralUS). This will fail if you get it wrong. If you're unsure, stop now and restart when you're certain."

New-AzDeployment -Name LightHouse -Location $Locale -TemplateFile ".\rgDelegatedResourceManagement.json" -TemplateParameterFile ".\rgDelegatedResourceManagement.parameters.json" -Verbose

Disconnect-AzAccount