# Azure Lighthouse

Lighthouse is another way for your technicians to access and manage client Azure subscriptions through the Azure portal.

These scripts will streamline the onboarding and deployment process. You will only need to run ```Get-ProviderAzureInfo.ps1``` one time. 
Then you can run ```Deploy-Lighthouse.ps1``` one time for each client you want to manage through Lighthouse.

The setup script will grab all the necessary information from your tenant and create a the required JSON Parameters file in the same directory.
This file is required for the deployment script.
The script will also print all retrieved info to the ```AzureLicenseInfo.txt``` file in the same directory.

These scripts will not work with Multi-Factor Authentication. The safest way to run them would be to disable MFA (In Azure AD) for only the account used. Run the scripts, then re-enable MFA on that account and change the password.

## Prerequisites

1. You must have Azure Powershell Module installed: ```Install-Module -Name Az```

2. Read and understand this [walkthrough][3]. You must have the proper role and group in place in your tenant.

3. The tenant admin account you login with when prompted MUST have an owner role on the client's Azure subscription.
Use [This][1] link if you're unfamiliar with how to add the role.
You can revoke that role permission once the deployment is completed.

4. The script has empty variables for login credentials. You'll be prompted if they're empty. This is in the case it's a pain retrieve those. Make sure you remove them after you're done if you do choose to hardcode them.

5. You should know the Name and ID of the primary subscription that your company will manage. You'll be prompted with a list of all the client's Azure subscriptions to choose from. The one you choose will be onboarded into your company's Lighthouse.

### Step 1: Make Sure Your AdminAgents Group is Configured inside YOUR Azure AD Instance
Any members of your team that you want to manage your client's Azure resources should be members of this group.

### Step 2: Download this directory
Download this directory to your machine. Open a Powershell terminal in it. 

### Step 3: Run the setup script.
Run ```.\Get-ProviderAzureInfo.ps1```

### Step 4: Run the deployment script
Run ```.\Deploy-Lighthouse.ps1```

## After deployment
Sign into [Azure][2] with your own credentials and search for ```My Customers (Azure Lighthouse)``` to see the clients available for management.

[1]: https://docs.microsoft.com/bs-latn-ba/azure/billing/billing-add-change-azure-subscription-administrator
[2]: https://portal.azure.com
[3]: https://www.2azure.nl/2019/07/18/how-to-setup-azure-lighthouse-manual/