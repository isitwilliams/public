Import-Module az

$user = ''
$password = ConvertTo-SecureString '' -AsPlainText -Force
if ($user.Length -gt 4) {
    $creds = New-Object System.Management.Automation.PSCredential($user,$password)
} else {
    Write-Host 'Login with Azure Credentials'
    $creds = Get-Credential
}

Connect-AzAccount -Credential $creds

$datafile = '.\AzureLicenseInfo.txt'

$companyName = Read-Host "What is your company name? (This is what will be displayed to your clients in the Azure Portal)."

Function ChooseSubscription {
    $subs = Get-AzSubscription
    Write-Host 'Choose a Subscription'
    For ($i=0; $i -lt $subs.Count; $i++) {
        Write-Host "$($i+1): $($subs[$i].Name) : ID: $($subs[$i].Id)"
    }
    [int]$selection = Read-Host "Select the number for the subscription you want."
    Write-Host "You selected $($subs[$selection-1])."
    $id = $($subs[$selection-1])
    return $id
}

$subId = ChooseSubscription

$sub = Get-AzSubscription -SubscriptionId $subId
Set-AzContext $sub

$tenantId = (Get-AzTenant).Id
"Company Tenant ID: " + $tenantId | Add-Content $datafile -Encoding Ascii 
Write-Host $tenantId

$groupId = (Get-AzADGroup -DisplayName 'AdminAgents').id
"Company Admin Agents Group ID: " + $groupId | Add-Content $datafile -Encoding Ascii

$roleId = (Get-AzRoleDefinition -Name 'Contributor').id
"Contributor Role ID: " + $roleId | Add-Content $datafile -Encoding Ascii

$JSON = [PSCustomObject]@{
    '$schema' = 'https://schema.management.azure.com/schemas/2015-01-01/deploymentParameters.json#'
    contentVersion = '1.0.0.0'
    parameters = [PSCustomObject]@{
        mspName = [PSCustomObject]@{
            value = $companyName
        }
        mspOfferDescription = [PSCustomObject]@{
            value = 'Azure Cloud Management'
        }
        managedByTenantId = [PSCustomObject]@{
            value = $tenantId
        }
        authorizations = [PSCustomObject]@{
            value = [PSCustomObject]@{
                principalId = $groupId
                roleDefinitionId = $roleId
                principalDisplayName = 'AdminAgents'
            }
        }
    }
}

$JsonOutput = ConvertTo-Json($JSON) -Depth 10

$jsonFile = '.\rgDelegatedResourceManagement.parameters.json'

$JsonOutput | Out-File -FilePath $jsonFile

Disconnect-AzAccount